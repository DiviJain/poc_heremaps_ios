//
//  HereMapCredModel.swift
//  RoutingApp
//
//  Created by Jain, Divya on 13/08/19.
//  Copyright © 2019 HERE Burnaby. All rights reserved.
//

import UIKit

struct HereMapCredModel {
    var appID :String
    var appKey : String
    var licenseKey : String
    
    init(appID:String,appKey: String,licenseKey: String) {
        self.appID    = appID
        self.appKey = appKey
        self.licenseKey = licenseKey
    }
}
