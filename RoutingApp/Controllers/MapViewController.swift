/*
 * Copyright (c) 2011-2019 HERE Europe B.V.
 * All rights reserved.
 */

import UIKit
import NMAKit
import CoreFoundation

// two Geo points for route.
let route1 = [
//    18.555887,73.765853
    
    NMAGeoCoordinates(latitude: 50.081938, longitude: 8.544004),//Industrial Park - South Gate (truck access)
//    NMAGeoCoordinates(latitude: 50.180454, longitude: 8.649856),//Industrial Park - South Gate (truck access)

    NMAGeoCoordinates(latitude: 50.174279, longitude: 8.642413),//ALCOA Industrial Chemicals Europa
    NMAGeoCoordinates(latitude: 50.122456, longitude: 8.676720),//Kühnreich & Meixner GmbH
    NMAGeoCoordinates(latitude: 50.112987, longitude: 8.692668)//Jin Dun Industrial GmbH


]

//let route2 = [
//    //    18.555887,73.765853
//    NMAGeoCoordinates(latitude: 19.191949, longitude: 72.888301),//Mumbai
//    NMAGeoCoordinates(latitude: 17.536129, longitude: 78.484526)//Hyderabad
//
//]

private class Defaults {
    static let latitude = 52.500556
    static let longitude = 13.398889
    
    static let width : Float = 0.01
    static let height: Float = 0.01
    
    static let imageName = "MarkerImg"
    static let radius = 250.0
    static let frame = CGRect(x: 110, y: 200, width: 220, height: 120)
    static let durationInterval = 2.0


}
let colorSchemeName = "color"
let floatSchemeName = "float"

class MapViewController: UIViewController {
    
//    public var hereMapCred: HereMapCredModel?
    let zoom = NMAZoomRange(minimum: 0, maximum: 20)
    var colorScheme: NMACustomizableScheme?
    var floatScheme: NMACustomizableScheme?
    var coreRouter: NMACoreRouter!
    var mapRouts = [NMAMapRoute]()
    var progress: Progress? = nil
    private var route : NMARoute?
    private var geoBoundingBox : NMAGeoBoundingBox?

    private var mapMarker : NMAMapMarker?

    // Get the NavigationManager instance.It is responsible for providing voice and visual
    // instructions while driving and walking.
    private lazy var navigationManager = NMANavigationManager.sharedInstance()
    @IBOutlet weak var mapView: NMAMapView!
    
    @IBOutlet weak var navigationControlButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        initValues()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        progress?.cancel()
    }
    
    /*
     Initialize CoreRouter and set center for map.
    */
    private func initValues() {
        coreRouter = NMACoreRouter()
        mapView.set(
            geoCenter: NMAGeoCoordinates(latitude: 49.260327, longitude: -123.115025),
            zoomLevel: 10, animation: NMAMapAnimation.none
        )
        navigationControlButton.setTitle("Start Navigation", for: .normal)
        navigationControlButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        // Set this controller to be the delegate of NavigationManager, so that it can listening to the
        // navigation events through the different protocols.In this example, we will
        // implement 2 protocol methods for demo purpose, please refer to HERE iOS SDK API documentation
        // for details
        navigationManager.delegate = self
    }
    
    @IBAction func clearMap(_ sender: UIButton) {
        // remove all routes from mapView.
        for route in mapRouts {
            mapView.remove(mapObject: route)
        }
        
        mapView.zoomLevel = 10
    }
    

    func addRoute(_ sender: UIButton) {
        
//        colorCustomization()
//        return;
        let routingMode = NMARoutingMode.init(
            routingType: NMARoutingType.fastest,
            transportMode: NMATransportMode.car,
            routingOptions: NMARoutingOption.avoidHighway
        )
        
//        routingMode.weightPerAxle = 24.0
//        routingMode.tunnelCategory = .b
        
        // check if calculation completed otherwise cancel.
        if !(progress?.isFinished ?? false) {
            progress?.cancel()
        }
        
        // store progress.
        progress = coreRouter.calculateRoute(withStops: route1, routingMode: routingMode, { (routeResult, error) in
            if (error != NMARoutingError.none) {
                NSLog("Error in callback: \(error)")
                return
            }
            
            guard let route = routeResult?.routes?.first else {
                print("Empty Route result")
                return
            }
//
//            guard let result = routeResult, let routes = result.routes, routes.count > 0 else {
//                print("Error:route result returned is not valid")
//                return
//            }
            
            // Let's add the 1st result onto the map
            self.route = route
            guard let mapRoute = NMAMapRoute.init(route) else {
                print("Can't init Map Route")
                return
            }
//            mapRoute.renderType = .userDefined
//                        mapRoute.upcomingColor = UIColor.gray
//                        mapRoute.traveledColor = UIColor.yellow
//                        mapRoute.outlineColor = UIColor.red
                        mapRoute.color = UIColor.red
            // In order to see the entire route, we orientate the
            // map view accordingly
     
            
            if (self.mapRouts.count != 0) {
                for route in self.mapRouts {
                    self.mapView.remove(mapObject: route)
                }
                self.mapRouts.removeAll()
            }
            
            if let boundingBox = route.boundingBox {
                self.geoBoundingBox = boundingBox
                self.mapView.set(boundingBox: boundingBox, animation: .linear)
                self.strtNav()
            }
            self.mapRouts.append(mapRoute)

//            self.mapView.set(boundingBox: box, animation: NMAMapAnimation.linear)
            self.mapView.add(mapObject: mapRoute)
            //create NMAImage with local cafe.png
            let markerImage = NMAImage(uiImage: UIImage(named: Defaults.imageName)!)
            //create NMAMapMarker located with geo coordinate and icon image
            self.mapMarker = markerImage.map{ NMAMapMarker(geoCoordinates: route1.last!, icon: $0) }
            self.mapMarker?.isDraggable = true
            //add NMAMapMarker to map view
            _ = self.mapMarker.map{ self.mapView.add(mapObject: $0) }
        })
        
        
    }

    @IBAction func startNavigation(_ sender: Any) {
        // To start a turn-by-turn navigation, a concrete route object is required.We use same steps
        // from Routing sample app to create a route from 4350 Still Creek Dr to Langley BC without
        // going on HWY
        // The route calculation requires local map data.Unless there is pre-downloaded map
        // data on device by utilizing MapLoader APIs,it's not recommended to trigger the
        // route calculation immediately after the MapEngine is initialized.The
        // NMARoutingErrorInsufficientMapData error code may be returned by CoreRouter in this case.
        
        if self.route == nil {
            addRoute(sender as! UIButton)
            return
        }
        
        
        navigationManager.stop()
        
        if !(NMAPositioningManager.sharedInstance().dataSource is NMADevicePositionSource) {
            NMAPositioningManager.sharedInstance().dataSource = nil
        }
        
        // Restore the map orientation to show entire route on screen
        self.geoBoundingBox.map{ mapView.set(boundingBox: $0, animation: .linear) }
        mapView.orientation = 0
        navigationManager.mapTrackingAutoZoomEnabled = false
        navigationManager.mapTrackingEnabled = false
        navigationControlButton.setTitle("Start Navigation", for: .normal)
        
        route = nil;
    }
    
    private func strtNav()
    {

        navigationControlButton.setTitle("Stop Navigation", for: .normal)
        // Display the position indicator on map
        mapView?.positionIndicator.isVisible = true
        // Configure NavigationManager to launch navigation on current map
        navigationManager.map = mapView
        
        let alert = UIAlertController(title: "Choose Navigation mode",
                                      message: "Please choose a mode",
                                      preferredStyle: .alert)
        
        //Add Buttons
        
        let deviceButton = UIAlertAction(title: "Navigation",
                                         style: .default) { _ in
                                            
                                            guard let route = self.route else {
                                                return
                                            }
                                            
                                            // Start the turn-by-turn navigation.Please note if the transport mode of the passed-in
                                            // route is pedestrian, the NavigationManager automatically triggers the guidance which is
                                            // suitable for walking.
                                            self.navigationManager.startTurnByTurnNavigation(route)
                                            
                                            // Set the map tracking properties
                                            self.navigationManager.mapTrackingEnabled = true
                                            self.navigationManager.mapTrackingAutoZoomEnabled = true
                                            self.navigationManager.mapTrackingOrientation = .dynamic
                                            self.navigationManager.isSpeedWarningEnabled = true
        }
        
        let simulateButton = UIAlertAction(title: "Simulation",
                                           style: .default) { _ in
                                            
                                            guard let route = self.route else {
                                                return
                                            }
                                            
                                            // Simulation navigation by init the PositionSource with route and set movement speed
                                            let source = NMARoutePositionSource(route: route)
                                            source.movementSpeed = 60
                                            NMAPositioningManager.sharedInstance().dataSource = source
                                            // Set the map tracking properties
                                            self.navigationManager.mapTrackingEnabled = true
                                            self.navigationManager.mapTrackingAutoZoomEnabled = true
                                            self.navigationManager.mapTrackingOrientation = .dynamic
                                            self.navigationManager.isSpeedWarningEnabled = true
                                            self.navigationManager.startTurnByTurnNavigation(route)
        }
        
        alert.addAction(deviceButton)
        alert.addAction(simulateButton)
        
        present(alert, animated: true, completion: nil)
    }
    private func showMessage(_ message: String) {
        let label = UILabel(frame: Defaults.frame)
        label.backgroundColor = UIColor.groupTableViewBackground
        label.textColor = UIColor.blue
        label.text = message
        label.numberOfLines = 0;
        
        let text = label.text! as NSString
        let rect = text.boundingRect(with: Defaults.frame.size,
                                     options: [.usesLineFragmentOrigin, .usesFontLeading],
                                     attributes: [NSAttributedString.Key.font : label.font],
                                     context: nil)
        var frame = Defaults.frame
        frame.size = rect.size
        label.frame = frame
        
        view.addSubview(label)
        
        UIView.animate(withDuration: Defaults.durationInterval, animations: {
            label.alpha = 0
        }) { _ in
            label.removeFromSuperview()
        }
    }
    
    func colorCustomization() {
        //if customized map scheme already exists, remove it first.
        if self.mapView.getCustomizableScheme(floatSchemeName) != nil {
            //it is not allowed to remove map scheme which is active.
            //set to other map scheme then remove.
            self.mapView.mapScheme = NMAMapSchemeNormalDay
            self.mapView.removeCustomizableScheme(floatSchemeName)
        }
        
        //create customizable scheme with specific scheme name based on NMAMapSchemeNormalDay
        if (colorScheme == nil) {
            colorScheme = self.mapView.createCustomizableScheme(colorSchemeName, basedOn: NMAMapSchemeNormalDay)
        }
        
        //create customizable color for property NMASchemeBuildingColor for specific zoom level
        let buildingColor = colorScheme?.colorForProperty(NMASchemeColorProperty.buildingColor, zoomLevel: 18.0)
        
        buildingColor?.red = 100
        buildingColor?.green = 100
        buildingColor?.blue = 133
        
        //set color property
        if let color = buildingColor {
            colorScheme?.setColorProperty(color, zoomRange: zoom)
        }
        
        //set map scheme to be customized scheme
        self.mapView.mapScheme = colorSchemeName
        self.mapView.set(geoCenter: NMAGeoCoordinates(latitude: 52.500556, longitude: 13.398889), zoomLevel: 18, animation: NMAMapAnimation.none)
    }
    
    func floatCustomization() {
        //if customized map scheme already exists, remove it first.
        if self.mapView.getCustomizableScheme(colorSchemeName) != nil {
            //it is not allowed to remove map scheme which is active.
            //set to other map scheme then remove.
            self.mapView.mapScheme = NMAMapSchemeNormalDay
            self.mapView.removeCustomizableScheme(colorSchemeName)
        }
        
        //create customizable scheme with specific scheme name based on NMAMapSchemeNormalDay
        if (floatScheme == nil) {
            floatScheme = self.mapView.createCustomizableScheme(floatSchemeName, basedOn: NMAMapSchemeNormalDay)
        }
        
        //set its float property boundary width to be 10.0 for specific zoom range
        floatScheme?.setFloatProperty(NMASchemeFloatProperty.countryBoundaryWidth, value: 10, zoomRange: zoom)
        
        //set map scheme to be customized scheme
        self.mapView.mapScheme = floatSchemeName
        self.mapView.zoomLevel = 4.0
    }
    
}

extension MapViewController : NMANavigationManagerDelegate {
    
    
    // Signifies that there is new instruction information available
    func navigationManager(_ navigationManager: NMANavigationManager, didUpdateManeuvers currentManeuver: NMAManeuver?, _ nextManeuver: NMAManeuver?) {
        showMessage("New maneuver is available")
        
    }
    
    // Signifies that the system has found a GPS signal
    func navigationManagerDidFindPosition(_ navigationManager: NMANavigationManager) {
        showMessage("New position has been found")
    }
    
}
