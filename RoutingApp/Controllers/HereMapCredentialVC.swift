//
//  HereMapCredentialVC.swift
//  RoutingApp
//
//  Created by Jain, Divya on 13/08/19.
//  Copyright © 2019 HERE Burnaby. All rights reserved.
//

import UIKit
import NMAKit

class HereMapCredentialVC: UIViewController {
    @IBOutlet weak var txtAppID: UITextField!
    @IBOutlet weak var txtAppKey: UITextField!
    @IBOutlet weak var txtLicenseKey: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    @IBAction func NextAction(_ sender: Any) {
        
        guard let appID = txtAppID.text, let appKey =  txtAppKey.text, let licenseKey = txtLicenseKey.text else {
            print("Invalid values")
            return
        }
        
        let hereMapCred = HereMapCredModel(appID: appID, appKey: appKey, licenseKey: licenseKey)
//        let hereMapCred = HereMapCredModel(appID: "L1KC3CIoBnkI0zsfXpf1", appKey: "Iq9H7KHbPFtlzrKQwqTPDg", licenseKey: "GhOoyoBkOLl3vBwZv94DfQxCH7uok3Ch2qvoSNn8yKFbRgvge770klCi/W8Xa4vROrjZuQusLNfeznRUglANZ7L1+Ad/YWsnhR0RxrkBmGf8BZgggZItU/mOLpEa1CVZ/b4/Kya6rYpImTdf6LbBxS1XOCBK95C4wdh7IFT8GgVB4O4jiGA/iZX3sToOgygLSQQzNMyK+AtIMlMrexkOLMH18U2i+Cq0zSOdHAzKFUNf4s/ogjSVhXYIdqQmT/7N/Jw9P8l+N2ep3tWCz6MUMwkkNEvFmJBM4VSSAZATxZ6DnXYbx/LNuvd/cvEJf0HmT4sFd2KIelXdF/7DizvYAuHFzlmbw/HCWXkpAmvrahConem7gdQCpjEKVacZ09LPVWjNiiIGYp+8DHJvAxRqi7c/jEFoQMgOCdxvROVCtIC/oP/BstRDXDvVPcLdhNvy9BFQIm4smsd2aMr0uwiYxJQkM9+VttOJuqYn1DUD3pk4QVD91aBi/T7URdweX0hW29MkutuPO7HSoBLLx6wl+T1TLUd0WFUndjfom+6BXMNEVPDWjOX0XNZedIeTyvEoYbFOo51Gb/IrifLfBLZZ5JJhZo9BAz2FyKonFHWh2Q7lGaQayGLbuiEvJK/vRMtBbS4Q7c/yXAnWsXSFkDXHGG4Lr3bRhtX+FllbdBG6Bmk=")

        NMAApplicationContext.setAppId(hereMapCred.appID, appCode: hereMapCred.appKey, licenseKey: hereMapCred.licenseKey)
        
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
            self.moveToMapVC()
//        })

    }
    
    
    func moveToMapVC() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.present(nextViewController, animated:true, completion:nil)
    }
}
